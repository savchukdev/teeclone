package main

import "vendor:sdl2"
import "vendor:sdl2/image"
import "vendor:sdl2/ttf"
import "core:log"
import "core:os"
import "core:slice"
import "core:mem"
import "core:math"
import "core:c"
import "core:math/linalg"
import "core:strings"
import "core:fmt"
import "core:strconv"
import "core:time"

render :: proc() {
  sdl2.SetRenderDrawColor(
    renderer = ctx.renderer,
    r = 110,
    g = 132,
    b = 174,
    a = 0xff,
  )
  sdl2.RenderClear(ctx.renderer)

  render_player()
  render_map()
  render_score()

  sdl2.RenderPresent(ctx.renderer)
}

render_score :: proc() {
  using ctx.time_tracker

  white := sdl2.Color{255, 255, 255, 255}

  if start != nil {
    duration := time.since(start.?)
    label := fmt.tprintf("Current time: %v", format_duration(duration))
    render_text(label, 50, 100, white)
  } else if finished_in != nil {
    label := fmt.tprintf("Finished in: %v", format_duration(finished_in.?))
    render_text(label, 50, 100, white)
  }

  if best > 0 {
    label := fmt.tprintf("Best time: %v", format_duration(best))
    render_text(label, 50, 150, white)
  }
}

// allocates on temporary allocator
format_duration :: proc(d: time.Duration) -> string {
  hours, minutes, seconds := time.clock(d)
  return fmt.tprintf("%02d:%02d:%02d", hours, minutes, seconds)
}

render_player :: proc() {
  BodyPart :: struct {
    source: sdl2.Rect,
    destination: sdl2.Rect,
    flip: sdl2.RendererFlip,
  }

  player_x := ctx.player.position.x - ctx.camera.x
  player_y := ctx.player.position.y - ctx.camera.y

  body_parts := [?]BodyPart{
    {rect(192, 64, 64, 32), rect(player_x-60, player_y, 96, 48), sdl2.RendererFlip.NONE}, // back feet shadow
    {rect(96, 0, 96, 96), rect(player_x-48,player_y-48, 96, 96), sdl2.RendererFlip.NONE}, // body shadow
    {rect(192, 64, 64, 32), rect(player_x-36, player_y, 96, 48), sdl2.RendererFlip.NONE}, // front feet shadow
    {rect(192, 32, 64, 32), rect(player_x-60, player_y, 96, 48), sdl2.RendererFlip.NONE}, // back feet
    {rect(0, 0, 96, 96), rect(player_x-48, player_y-48, 96, 96), sdl2.RendererFlip.NONE}, // body
    {rect(192, 32, 64, 32), rect(player_x-36, player_y, 96, 48), sdl2.RendererFlip.NONE}, // front feet
    {rect(64, 96, 32, 32), rect(player_x-18, player_y-21, 36, 36), sdl2.RendererFlip.NONE}, // left eye
    {rect( 64,  96, 32, 32), rect(player_x-6, player_y-21, 36, 36), sdl2.RendererFlip.HORIZONTAL}, //ight eye
  }

  for part in body_parts {
    source := part.source
    destination := part.destination

    sdl2.RenderCopyEx(
      ctx.renderer,
      ctx.player.texture,
      &source,
      &destination,
      0,
      nil,
      part.flip,
    )
  }
}

render_map :: proc() {
  source := rect(0, 0, TILE_SIZE.x, TILE_SIZE.y)
  destination := rect(0, 0, TILE_SIZE.x, TILE_SIZE.y)

  for tile, i in ctx.world_map.tiles {
    if tile == 0 { continue }

    source.x = i32(tile) % TILES_PER_ROW * TILE_SIZE.x
    source.y = i32(tile) / TILES_PER_ROW * TILE_SIZE.y

    destination.x = i32(i) % ctx.world_map.width * TILE_SIZE.x - ctx.camera.x
    destination.y = i32(i) / ctx.world_map.width * TILE_SIZE.y - ctx.camera.y

    sdl2.RenderCopy(ctx.renderer, ctx.world_map.texture, &source, &destination)
  }
}

TextTexture :: struct {
  texture: ^sdl2.Texture,
  w, h: i32,
}

TextCacheKey :: struct {
  text: string,
  outline: i32,
}

text_cache : map[TextCacheKey]TextTexture

render_text_raw :: proc(font: ^ttf.Font, text: string, x, y, outline: i32, color: sdl2.Color) {
  cache_texture, ok := text_cache[TextCacheKey{text, outline}]
  if !ok {
    c_text := strings.clone_to_cstring(text)
    defer delete(c_text)

    ttf.SetFontOutline(font, outline)
    text_surface := ttf.RenderUTF8_Blended(font, c_text, color)
    if text_surface == nil {
      log.errorf("Could not render text: %v", ttf.GetError())
      return
    }
    defer sdl2.FreeSurface(text_surface)

    sdl2.SetSurfaceAlphaMod(text_surface, color.a)

    texture := sdl2.CreateTextureFromSurface(ctx.renderer, text_surface)
    if texture == nil {
      log.errorf("Could not create text texture: %v", ttf.GetError())
      return
    }

    cache_texture = TextTexture{texture, text_surface.w, text_surface.h}
    text_cache[TextCacheKey{text, outline}] = cache_texture
  }

  source := rect(0, 0, cache_texture.w, cache_texture.h)
  destination := rect(x - outline, y - outline, cache_texture.w, cache_texture.h)

  sdl2.RenderCopyEx(
    ctx.renderer,
    cache_texture.texture,
    &source,
    &destination,
    0,
    nil,
    sdl2.RendererFlip.NONE,
  )
}

render_text_simple :: proc(text: string, x, y: i32, color: sdl2.Color) {
  outline_color := color
  outline_color.a = outline_color.a / 4
  render_text_raw(ctx.font, text, x, y, 2, outline_color)
  render_text_raw(ctx.font, text, x, y, 0, color)
}

render_text :: proc{render_text_raw, render_text_simple}