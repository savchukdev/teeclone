package main

import "vendor:sdl2"
import "vendor:sdl2/image"
import "vendor:sdl2/ttf"
import "core:log"
import "core:os"
import "core:slice"
import "core:mem"
import "core:math"
import "core:c"
import "core:math/linalg"
import "core:strings"
import "core:fmt"
import "core:strconv"
import "core:time"

WINDOW_TITLE :: "Platformer"
WINDOW_WIDTH := i32(1280)
WINDOW_HEIGHT := i32(720)

WINDOW_X := i32(0)
WINDOW_Y := i32(0)

WINDOW_FLAGS :: sdl2.WindowFlags{.SHOWN}

TILES_PER_ROW :: 16
TILE_SIZE :: sdl2.Point{64, 64}

PHYSICS_TICK_TIME := time.duration_milliseconds(time.Second * 1./60.)

AIR_BLOCK :: 0
START_BLOCK :: 78
FINISH_BLOCK :: 110
NON_SOLID_BLOCKS :: []int{AIR_BLOCK, START_BLOCK, FINISH_BLOCK}

PLAYER_SIZE :: linalg.Vector2f32{64, 64}

Player :: struct {
  texture: ^sdl2.Texture,
  position: sdl2.Point,
  velocity: linalg.Vector2f32,
}

WorldMap :: struct {
  texture: ^sdl2.Texture,
  width, height: i32,
  tiles: []u8,
}

Inputs :: [len(Input)]bool

TimeTracker :: struct {
  start: Maybe(time.Time),
  finished_in: Maybe(time.Duration),
  best: time.Duration,
}

CTX :: struct {
  window: ^sdl2.Window,
  renderer: ^sdl2.Renderer,
  player: Player,
  world_map: WorldMap,
  camera: sdl2.Point,

  time_tracker: TimeTracker,

  inputs: Inputs,
  previous_inputs: Inputs,

  should_close: bool,
  app_start: f64,

  frame_start: f64,
  frame_end: f64,
  frame_elapsed: f64,

  font: ^ttf.Font,
}

Input :: enum {
  None,
  Left,
  Right,
  Jump,
  Restart,
  Quit,
}

ctx := CTX{}

main :: proc() {
  context.logger = log.create_console_logger()

  if res := init_sdl(); !res {
    log.errorf("Initialization failed.")
    os.exit(1)
  }

  defer cleanup()

  if !init() {
    os.exit(1)
  }

  ctx.app_start = f64(sdl2.GetPerformanceCounter()) / f64(sdl2.GetPerformanceFrequency())

  loop()

  now := f64(sdl2.GetPerformanceCounter()) / f64(sdl2.GetPerformanceFrequency())
  elapsed := now - ctx.app_start

  log.infof("Finished in %v seconds\n", elapsed)
}

init_sdl :: proc() -> bool {
  if sdl_res := sdl2.Init(sdl2.INIT_VIDEO); sdl_res < 0 {
    log.errorf("sdl2.init returned %v.", sdl2.GetError())
    return false
  }

  bounds := sdl2.Rect{}
  if e := sdl2.GetDisplayBounds(0, &bounds); e != 0 {
    log.errorf("Unable to get display bounds: %v", sdl2.GetError())
    return false
  }

  if e := ttf.Init(); e != 0 {
    log.errorf("Unable initialize sdl2 ttf: %v", sdl2.GetError())
    return false
  }

  image.Init(image.INIT_PNG)

  WINDOW_X = ((bounds.w - bounds.x) / 2) - (WINDOW_WIDTH / 2) + bounds.x
  WINDOW_Y = ((bounds.h - bounds.y) / 2) - (WINDOW_HEIGHT / 2) + bounds.y

  ctx.window = sdl2.CreateWindow(
    title = WINDOW_TITLE,
    x = WINDOW_X,
    y = WINDOW_Y,
    w = WINDOW_WIDTH,
    h = WINDOW_HEIGHT,
    flags = WINDOW_FLAGS,
  )
  if ctx.window == nil {
    log.errorf("sdl2.CreateWindow failed")
    return false
  }

  ctx.renderer = sdl2.CreateRenderer(ctx.window, -1, {.ACCELERATED, .PRESENTVSYNC})
  if ctx.renderer == nil {
    log.errorf("sdl2.CreateRenderer failed")
    return false
  }

  return true
}

cleanup :: proc() {
  if ctx.player.texture != nil {
    sdl2.DestroyTexture(ctx.player.texture)
  }

  if ctx.world_map.texture != nil {
    sdl2.DestroyTexture(ctx.world_map.texture)
  }

  if ctx.world_map.tiles != nil {
    delete(ctx.world_map.tiles)
  }

  for _, texture in text_cache {
    sdl2.DestroyTexture(texture.texture)
  }
  delete(text_cache)

  sdl2.DestroyRenderer(ctx.renderer)
  sdl2.DestroyWindow(ctx.window)
  ttf.CloseFont(ctx.font)
  image.Quit()
  ttf.Quit()
  sdl2.Quit()
}

init :: proc() -> bool {
  text_cache = make(map[TextCacheKey]TextTexture)

  return init_player() &&
    init_world() &&
    init_fonts()
}

get_map_tile :: proc(x, y: i32) -> u8 {
  world_map := ctx.world_map

  nx := clamp(x / TILE_SIZE.x, 0, world_map.width - 1)
  ny := clamp(y / TILE_SIZE.y, 0, world_map.height - 1)

  index := ny * world_map.width + nx

  return world_map.tiles[index]
}

get_player_tile :: proc() -> u8 {
  return get_map_tile(ctx.player.position.x, ctx.player.position.y)
}

is_solid_tile :: proc(x, y: i32) -> bool {
  target_tile := int(get_map_tile(x, y))
  return !slice.contains(NON_SOLID_BLOCKS, target_tile)
}

is_in_solid :: proc(x, y: i32) -> bool {
  size_x := i32(PLAYER_SIZE.x / 2)
  size_y := i32(PLAYER_SIZE.y / 2)

  return is_solid_tile(x + size_x, y + size_y) ||
         is_solid_tile(x - size_x, y + size_y) ||
         is_solid_tile(x + size_x, y - size_y) ||
         is_solid_tile(x - size_x, y - size_y)
}

is_player_on_ground :: proc() -> bool {
  size_x := i32(PLAYER_SIZE.x / 2)
  size_y := i32(PLAYER_SIZE.y / 2)

  player_x := ctx.player.position.x
  player_y := ctx.player.position.y

  return is_solid_tile(player_x - size_x, player_y + size_y + 1) ||
    is_solid_tile(player_x + size_x, player_y + size_y + 1)
}

keycode_to_input :: proc(key: sdl2.Keycode) -> Input {
  #partial switch key {
    case .SPACE:
      return .Jump
    case .A:
      return .Left
    case .D:
      return .Right
    case .R:
      return .Restart
    case .Q:
      return .Quit
  }
  return .None
}

process_input :: proc() {
  event: sdl2.Event

  mem.copy(&ctx.previous_inputs, &ctx.inputs, size_of(Inputs))

  for sdl2.PollEvent(&event) {
    #partial switch(event.type) {
      case .QUIT:
        ctx.inputs[Input.Quit] = true
      case .KEYDOWN:
        #partial switch(event.key.keysym.sym) {
          case .ESCAPE:
            ctx.inputs[Input.Quit] = true
        }

        ctx.inputs[keycode_to_input(event.key.keysym.sym)] = true
      case .KEYUP:
        ctx.inputs[keycode_to_input(event.key.keysym.sym)] = false
    }
  }

  if ctx.inputs[Input.Restart] {
    restart_player()
  }
}

rect :: proc(x, y, w, h: c.int) -> sdl2.Rect {
  return sdl2.Rect{x, y, w, h}
}

move_camera :: proc() {
  distance_to_camera := f32(ctx.camera.x - ctx.player.position.x + WINDOW_WIDTH / 2)
  ctx.camera.x -= i32(0.05 * distance_to_camera)
}

logic :: proc() {
  switch get_player_tile() {
    case START_BLOCK:
      ctx.time_tracker.start = time.now()
      ctx.time_tracker.finished_in = nil
    case FINISH_BLOCK:
      if ctx.time_tracker.start != nil {
        assert(ctx.time_tracker.start != nil, "Start value must be there")
        start := ctx.time_tracker.start.?
        finish := time.now()
        duration := time.diff(start, finish)

        if duration < ctx.time_tracker.best || ctx.time_tracker.best == 0 {
          ctx.time_tracker.best = duration
        }

        ctx.time_tracker.finished_in = time.diff(start, finish)
        ctx.time_tracker.start = nil
      }
  }
}

loop :: proc() {
  ctx.frame_start = ctx.app_start
  ctx.frame_elapsed = 0.001

  accumulated := 0.0

  physics_time := time.now()

  for !ctx.inputs[Input.Quit] {
    new_physics_time := time.now()
    frame_time := time.duration_milliseconds(time.diff(physics_time, new_physics_time))
    physics_time = new_physics_time

    accumulated += frame_time

    for accumulated >= PHYSICS_TICK_TIME {
      run_physics()
      move_camera()
      logic()

      accumulated -= PHYSICS_TICK_TIME
    }

    process_input()
    render()

    ctx.frame_end = f64(sdl2.GetPerformanceCounter()) / f64(sdl2.GetPerformanceFrequency())
    ctx.frame_elapsed = ctx.frame_end - ctx.frame_start
    ctx.frame_start = ctx.frame_end

    free_all(context.temp_allocator)
  }
}

restart_player :: proc() {
  ctx.player.position = sdl2.Point{170, 500}
  ctx.player.velocity = linalg.Vector2f32{0, 0}
  ctx.time_tracker.start = nil
  ctx.time_tracker.finished_in = nil
}

PLAYER_PNG := #load("assets/player.png")
init_player :: proc() -> bool {
  restart_player()
  ctx.player.texture = load_texture_from_mem(PLAYER_PNG, i32(len(PLAYER_PNG)))

  return ctx.player.texture != nil
}

FONT_FILE := #load("assets/DejaVuSans.ttf")
init_fonts :: proc() -> bool {
  rw := sdl2.RWFromMem(rawptr(&FONT_FILE[0]), i32(len(FONT_FILE)))
  ctx.font = ttf.OpenFontRW(rw, true, 28)
  if ctx.font == nil {
    log.errorf("Could not open font file: %v", ttf.GetError())
    return false
  }

  return true
}

GRASS_PNG := #load("assets/grass.png")
DEFAULT_MAP := #load("assets/default.map")
init_world :: proc() -> bool {
  ctx.world_map.texture = load_texture_from_mem(GRASS_PNG, i32(len(GRASS_PNG)))
  if ctx.world_map.texture == nil {
    return false
  }

  data := string(DEFAULT_MAP)

  width := 0
  height := 0
  tiles := make([dynamic]u8, 0, 256)

  for line, line_number in strings.split_lines(string(data)) {
    width = 0
    for element in strings.split(line, " ") {
      if element == "" { continue }
      number, ok := strconv.parse_uint(element)
      if !ok {
        log.error("World map contains invalid number (not u8)")
        return false
      }

      if i32(number) > c.UINT8_MAX {
        log.error("World map contains too large of a number (greater than u8)")
        return false
      }

      append(&tiles, u8(number))
      width += 1
    }

    if ctx.world_map.width > 0 && ctx.world_map.width != i32(width) {
        log.errorf("Line %v inconsistent amount of tiles", line_number)
        return false
    }
    ctx.world_map.width = i32(width)

    height += 1
  }

  ctx.world_map.height = i32(height)
  ctx.world_map.width = i32(width)
  ctx.world_map.tiles = tiles[:]

  return true
}

load_texture_from_mem :: proc(data: []u8, size: c.int) -> ^sdl2.Texture {
  rw := sdl2.RWFromMem(rawptr(&data[0]), size)

  texture := image.LoadTexture_RW(ctx.renderer, rw, true)
  if texture == nil {
    log.errorf("Couldn't load texture: %v", image.GetError())
    return nil
  }

  return texture
}