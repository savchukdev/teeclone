package main

import "vendor:sdl2"
import "vendor:sdl2/image"
import "vendor:sdl2/ttf"
import "core:log"
import "core:os"
import "core:slice"
import "core:mem"
import "core:math"
import "core:c"
import "core:math/linalg"
import "core:strings"
import "core:fmt"
import "core:strconv"
import "core:time"

run_physics :: proc() {
  ground := is_player_on_ground()

  if ctx.inputs[Input.Jump] {
    if ground {
      ctx.player.velocity.y = -21.
    }
  }

  ctx.player.velocity.y += 0.75

  direction := f32(int(ctx.inputs[Input.Right]) - int(ctx.inputs[Input.Left]))

  if ground {
    ctx.player.velocity.x = 0.5 * ctx.player.velocity.x + 3.0 * direction
  } else {
    ctx.player.velocity.x = 0.6 * ctx.player.velocity.x + 5.0 * direction
  }

  ctx.player.velocity.x = clamp(ctx.player.velocity.x, -8, 8)

  move_player()
}

move_player :: proc() {
  distancef32 := linalg.length(ctx.player.velocity)
  distance := int(distancef32)

  if distance < 0 {
    return
  }

  fraction := 1. / (distancef32 + 1.)

  current_x := f32(ctx.player.position.x)
  current_y := f32(ctx.player.position.y)

  for i := 0; i < distance; i += 1 {
    new_x := current_x + ctx.player.velocity.x * fraction
    new_y := current_y + ctx.player.velocity.y * fraction

    if is_in_solid(i32(new_x), i32(new_y)) {
      hit := false

      if is_in_solid(i32(current_x), i32(new_y)) {
        hit = true
        new_y = current_y
        ctx.player.velocity.y = 0
      }

      if is_in_solid(i32(new_x), i32(current_y)) {
        hit = true
        new_x = current_x
        ctx.player.velocity.x = 0
      }

      if !hit {
        new_x = current_x
        new_y = current_y
        ctx.player.velocity = linalg.Vector2f32{0, 0}
      }
    }

    current_x = new_x
    current_y = new_y
  }

  ctx.player.position.x = i32(current_x)
  ctx.player.position.y = i32(current_y)
}